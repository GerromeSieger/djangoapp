from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .forms import RegForm
from .models import Epl, Laliga, SerieA, League
from django.contrib.auth.models import User


def home(request):
    return render(request, "main/home.html")

def register(request):
    if request.method == "POST":
        form = RegForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('login')

    else:
        form = RegForm()
    return render(request, "main/register.html", {"form":form})

def about(request):
    return render(request, "main/about.html")

def contact(request):
    return render(request, "main/contact.html")

def news(request):
    return render(request, "main/news.html")

def footballers(request):
    return render(request, "main/footballers.html")

def post(request):
    return render(request, "main/post.html")

def leagues(request):
    club = League.objects.all()
    return render(request, "main/leagues.html", {"club":club})

def singlepost(request):
    return render(request, "main/singlepost.html")
